const express = require('express');
const app = express();
const port = 3000;
const hbs = require("hbs");
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/partials");

app.get("/", (req, res) => {
    res.send("Hello, Express!");
});

app.get("/login", (req, res) => {
    res.send("This is a login page");
});

// Обробник маршруту /weather (класичний URL) 
app.get("/weather", (req, res) => {
    const city = req.query.city;
    if (city) {
        res.send(city);
    }
    else {
        const weather = {
            description: "Clear Sky"
        }
        res.render("weather", {weather});
    }
});

// Обробник маршруту /weather/:city (семантичний URL)
app.get("/weather/:city", (req, res) => {
    const city = req.params.city;
    res.send(city);
});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
})