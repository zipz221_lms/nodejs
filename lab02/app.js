const express = require('express');
const app = express();
const port = 3000;
const hbs = require("hbs");
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/partials");
const axios = require("axios");

const apikey = "940470f7d6a69f00aeb259729ef256a1";
const cities = ["Kyiv", "Warsaw"];

async function getWeatherData(city) {
    try {
        const response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apikey}&units=metric`); 
        return response.data;
    }
    catch (error) 
    { 
        console.error(error); throw new Error('Error fetching weather data'); 
    }
};

app.get("/weather", (req, res) => {
        res.render("weather", {cities});
    });

app.get("/weather/:city", async (req, res) => {
    const city = req.params.city;
    try {
    const data = await getWeatherData(city);
    const icon = `https://openweathermap.org/img/w/${data.weather[0].icon}.png`;
    const weather = {
       temperature: data.main.temp,
       pressure: data.main.pressure,
       humidity: data.main.humidity,
       icon: icon
    }
    res.json(weather);
    }
    catch (error) {
        res.status(500).send('Error fetching weather data');
    }
});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
})