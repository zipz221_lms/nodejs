const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, 'user.json');

const loadUser = () => {
    try {
        const dataBuffer = fs.readFileSync(filePath);
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(err) {
       console.error(err.message);
    }
};

const saveUser = (user) => {
  try {
    const dataToSave = JSON.stringify(user, null, 2);
    fs.writeFileSync(filePath, dataToSave, 'utf8');
    console.log('User data saved successfully.');
  } catch (err) {
    console.error(err.message);
  }
};

const addLanguage = (title, level) => {
  const user = loadUser();
  if (user) {
    const duplicateLanguages = user.languages.find( language => language.title.toLowerCase().includes(title.toLowerCase()) );
    if (!duplicateLanguages) {
        user.languages.push({
            title: title,
            level: level
        });
    
        saveUser(user);
        console.log(`Language "${title}" added successfully.`);
    } else {
        console.log(`Couldn't add language "${title}" already exists`);
    } 
  }
};

const removeLanguage = (title) => {
  const user = loadUser();

  if (user) {
    const final = user.languages.filter( language => language.title.toLowerCase() !== title.toLowerCase() );
    if (user.languages.length > final.length) {
        user.languages = final;
        console.log(`Language "${title}" removed successfully.`);
        saveUser(user);
    } else {
        console.log(`${title} not found`);
    }   
  }
};

const listLanguages = () => {
  const user = loadUser();
  if (user) {
    console.log('Languages:');
    user.languages.forEach(lang => {
      console.log(`- ${lang.title} (Level: ${lang.level})`);
    });
  }
};

const readLanguage = (title) => {
  const user = loadUser();
  if (user) {
    const language = user.languages.find(lang => lang.title.toLowerCase().includes(title.toLowerCase()));
    if (language)  {
      console.log(`Language: ${language.title}`);
      console.log(`Level: ${language.level}`);
    } else {
      console.log(`Language "${title}" not found.`);
    }
}};

module.exports = {
  addLanguage,
  removeLanguage,
  listLanguages,
  readLanguage
};