const lodash = require('lodash');

// 1. _.chunk(array, [size=1])
// Creates an array of elements split into groups the length of size. If array can't be split evenly, the final chunk will be the remaining elements.
let array = ['a', 'b', 'c', 'd'];
let result = lodash.chunk(array, 2);
console.log('Example 1: ', result); // => [['a', 'b'], ['c', 'd']]

// 2. _.compact(array)
// Creates an array with all falsey values removed. The values false, null, 0, "", undefined, and NaN are falsey.
array = [0, 1, false, 2, '', 3];
result = lodash.compact(array);
console.log('Example 2: ', result); // => [1, 2, 3]

// 3. _.concat(array, [values])
// Creates a new array concatenating array with any additional arrays and/or values.
array = [1];
result = lodash.concat(array, 2, [3], [[4]]);
console.log('Example 3: ', result); // => [1, 2, 3, [4]]

// 4. _.difference(array, [values])
// Creates an array of array values not included in the other given arrays using SameValueZero for equality comparisons. The order and references of result values are determined by the first array.
array = [2, 1];
result = lodash.difference(array, [2, 3]);
console.log('Example 4: ', result); // => [1]

// 5. _.drop(array, [n=1])
// Creates a slice of array with n elements dropped from the beginning.
array = [1, 2, 3];
result = lodash.drop(array);
console.log('Example 5: ', (result)); // => [2, 3]