const os = require('os');

const userName = os.userInfo({encoding: 'utf8'}).username;
const greeting = `Hello, ${userName}!`;

console.log(greeting);  

