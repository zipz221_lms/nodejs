const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, 'task02.txt');
const data = 'Hello, World!\n';


fs.appendFile(filePath, data, (err) => {
    if (err) {
        console.error("Помилка при додаванні до файлу:", err);
        
    }
    else {
    console.log("Рядок успішно додано до файлу.");
    }
});